import React from "react";
import icon from "../assets/img/Icon.png";
import layer from "../assets/img/Layer_10.png";
import presentation from "../assets/img/presentation.png";
import books from "../assets/img/reading-book.png";
function Section3() {
  const cards = [
    { logo: icon, largeText: "742+", des: "Active Learner" },
    { logo: layer, largeText: "65+", des: "Educators" },
    { logo: books, largeText: "X", des: "Course Available" },
    { logo: presentation, largeText: "X", des: "Communities Reached" },
  ];
  return (
    <div className="container section3">
      <h4>Empowering Minds, Impacting Communities</h4>
      <p>
        We’re becoming the premiere eLearning hub for remote workers and
        communities across the globe, offering localized content that <br />{" "}
        reflects their various cultures, languages, and learning styles.
      </p>
      <div className="main">
        {cards.map((card, i) => {
          return (
            <div className={`card_main _${card.largeText}`} key={i}>
              <img src={card.logo} alt="" />
              <h5>{card.largeText}</h5>
              <p>{card.des}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Section3;
