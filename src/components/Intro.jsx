import React from "react";
import logo from "../assets/img/logo-mini.png";
import splash from "../assets/img/text-bg.png";
import bg_logo from "../assets/img/big-bg-logo.png";
import banner_image from "../assets/img/banner-img.png";
function Intro() {
  return (
    <div className="container-fluid">
      <div className="row section">
        <div className="col-md-6">
          <div className="intro">
            <img src={logo} alt="" />
            <p className="first">
              Let’s build skills <br /> with{" "}
              <span
                style={{
                  backgroundImage: `url(${splash}) `,
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "cover",
                }}
              >
                IFA
              </span>{" "}
              & Learn <br />
              Without Limits...
            </p>
            <p>Take your Learning to next level.</p>
          </div>
        </div>
        <div className="col-md-6">
          <div className="background_logo">
            <img src={bg_logo} alt="" />
            <img src={banner_image} alt=""  className="over_wrapper"/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Intro;
