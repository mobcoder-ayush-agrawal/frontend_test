import React from "react";
import candidate from "../assets/img/learning-exp.png";
function Section2() {
  return (
    <div className="row">
      <div className="col-md-6">
        <img src={candidate} alt="" className="img-fluid" />
      </div>
      <div className="col-md-6 candidate_left">
        <h5>Learn Anywhere Anytime</h5>
        <h3>Positive Learning Experiences <br /> At Your Fingertips</h3>
        <p>
          Access digital educational content directly on your mobile device and <br />
          interact with a learning bot through any one of your preferred social <br />
          messaging platforms. Come collaborate with peers and educators from <br />
          around the world, and exchange knowledge and ideas as life- <br /> long
          learners.
        </p>
        <button className="btn btn-success">REGISTER AS LEARNER</button>
      </div>
    </div>
  );
}

export default Section2;
