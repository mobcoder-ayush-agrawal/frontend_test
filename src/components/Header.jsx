import React from "react";
import logo from "../assets/img/logo-white.png";
import map from "../assets/img/united-kingdom.png";
import Intro from "./Intro";
function Header() {
  return (
    <>
    <div className="header">
      <div className="container">
        <div className="logo">
          <img src={logo} alt="" />
        </div>
        <ul className="menus">
          <li>Home <span></span></li>
          <li>About us</li>
          <li>Course</li>
          <li>Contact</li>
        </ul>
        <div className="login">
          <img src={map} alt="" />
          <span>Login</span>
          <button className="btn btn-success">Get Started</button>
        </div>
      </div>
    </div>
    <Intro/>
    </>
  );
}

export default Header;
