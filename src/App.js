import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Section2 from './components/Section2';
import Section3 from './components/Section3';

function App() {
  return (
    <>
      <Header />
      <Section2 />
      <Section3 />

    </>
  );
}

export default App;
